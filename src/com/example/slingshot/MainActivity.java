package com.example.slingshot;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;

public class MainActivity extends Activity {
	final int TICKS_PER_SECOND = 25;
	final int SKIP_TICKS = 1000 / TICKS_PER_SECOND;
	final int MAX_FRAMESKIP = 5;
	private long startTime;
	private boolean game_is_running;
	
	private Bitmap target;
	private int targetX;
	private int targetY;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//The time the game was first launched
		startTime = System.currentTimeMillis();
		//Gets the target image from resources
		target = BitmapFactory.decodeResource(getResources(), R.drawable.target);
		targetX=500;
		targetY=500;
		game();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			this.finish();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}
	
	/*
	 * The Main heart beat of this video game
	 */
	public void game(){
		long next_game_tick = getTickCount();
    	int loops;
    	float interpolation;
    	
    	game_is_running = true;
    	//run forever or until the user decides to quit
    	while ( game_is_running){
    		//reset number of loops back to 0
    		loops = 0;
    		
    		/*
    		 * you can't update the game more than 5 times in a row
    		 */
    		while (getTickCount() > next_game_tick && loops < MAX_FRAMESKIP){
    			updateGame();
    			
    			next_game_tick += SKIP_TICKS;
    			++loops;
    		}
    		
    		//interpolation is the value the displayGame might be off, 
    		//if an update should have occurred .3 seconds ago for example
    		interpolation = (float)(getTickCount() + SKIP_TICKS - next_game_tick) / (float)(SKIP_TICKS);
    		
    		displayGame(interpolation);
    	}
	}
	
	//return the amount of milliseconds since game launched
	private long getTickCount(){
    	return (System.currentTimeMillis() - startTime);
    }
    
	//this function should only contain updates to the game's underlying functions
	//this is not called as frequently as displayGame()
    private void updateGame(){
    	Log.d("U", "update");
    }
    
    //this function should only change the images being displayed
    //this is called more often than updateGame
    private void displayGame(float interpolation){
    	Log.d("D", "display");
    	
    	//mLL.setB
    }

}